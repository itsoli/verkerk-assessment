# Verkerk Developer Assessment
  
Implement REST api based client-server solution


Expected duration: 3 - 4 hours; In scope of this assessment we expect you to develop small solution which implement two User Stories:


User Story 1:
As a Back Office administrator I have to manage companies and their vacancies by using a REST api. By using the api endpoints we should be able to retrieve, insert, update, delete data from the storage.

A company should at least have a name and an address.

A vacancy should at least have a title and a description.

User Story 2:

As a user I want to see list of companies with their vacancies who have at least one active vacancy available.

Technical expectations

- Simple .NET Core web API (proposed: .NET Core version 3.x or higher)

- Simple data storage (proposed: EntityFramework and SQL Server)

- Simple frontend with a call to the REST api (proposed: REACT site);

- optional: use the D3 library to show the vacancy on a map (creativity)
# Solution Structure
 
## Common Folder
There are two class libraries in this folder. Some common Class, Extensions, Exceptions and Models are defined here. 
 
**Note:** These class libraries should be nuget packages.
 
## Services Folder
### Vacancy Service
**Vacancy.API Project**
 
There are controllers that handles requests from the UI client which is a react client. On Project startup I Use `DbSeed.cs` to insert some data to db. I Used `SqlServer Express` for this project, however in production mode I rather use [postgres](https://www.postgresql.org/).
 
- I used `Serilog` for logging. you can find the configuration in `Program.cs` and `appsettings.json`
- In `appsettings.json` there is a setting part
    - AllowedCorsDomains: Allowed domains for cors.
    - JwtSettings_SecurityKey: Security Key of JWT
    - JwtSettings_Issuer: Issuer of JWT
- I have used a general Exception Handler Called `ExceptionMiddleware` for production exception handling.
 
 **Vacancy.UnitTests Project**
 
 Some UnitTest are tests included in this project, however since I had limited time I couldn't add more tests like `FunctionalTest`.
 
- I Used [XUnit](https://xunit.net/) and [Moq](https://github.com/moq/moq4) for writing tests.
 
### Identity Service
A simple identity service only for this assessment (Don't Try It At Home :D ). In production projects I rather using open source libraries like[Identityserver4](https://identityserver4.readthedocs.io/en/latest/) and [OpenIddict](https://github.com/openiddict/openiddict-core) or cloud services provided by Azure or Amazon.
 
 
# How to Build
 
## Backend
### Using Visual Studio
 
1. Clone the repo.
2. Open Visual Studio.
3. On the start window, select Open a project or solution.
4. Visual Studio opens an instance of File Explorer, where you can browse to your solution or project, and then select it to open it.
5. Right-click on solution & build.
6. Right-click on solution and select Properties
7. In Startup Project select `Multiple startup project`
8. Set `Vacancy.API` and `Identity.API` Action to `Start` and press ok.
9. Start the solution.
 
**Important Note:** `Vacancy.API` by default is using port 5000 and  `Identity.API` is using port 5001, if these ports are already in use on your system you can change them by editing `launchSettings.json` inside the Properties folder. If any changes were applied make sure that you change the '.env' file in React UI project.
 
### Using Docker
If you already have installed Docker Desktop you are a hero, otherwise you can install it [here](https://www.docker.com/products/docker-desktop)
 
1. Clone the repo.
2. Open Visual Studio.
3. On the start window, select Open a project or solution.
4. Visual Studio opens an instance of File Explorer, where you can browse to your solution or project, and then select it to open it.
5. Right-click on solution & build.
6. Right-click on the `docker-compose` file in the solution and select `set as startup project`.
7. Start the solution.(it may take a while to download and build docker images and containers)
 
**Important Note:** `Vacancy.API` by default is using port 5000 and  `Identity.API` is using port 5001, if these ports are already in use on your system you can change them by editing `docker-compose.yml` file by editing port mapping sections. If any changes were applied make sure that you change the '.env' file in the React UI project.
 
## UI
1. Install NodeJs LTS version from [NodeJs Official Page](https://nodejs.org/en/)
2. Clone the repo.
3. Open VS Code.
4. Open `Frontend` folder.
5. Open a new terminal.
6. Run in terminal `npm install` Then run `npm start` or you can simply run `npm run install:clean`.
 
**Important Note 1:** The UI by default is using port 3000, if this port is already in use you should add a new port to `AllowedCorsDomains` in `appsettings.json` to prevent CORS error.
 
**Important Note 2:** By using docker you cant access the database file since it's inside a docker container unless you add some volume mapping inside `docker-compose.yml` file.
 
 
# Road Map
 
1. Add more unit and functional tests.
2. Use a reliable identity server.
 

