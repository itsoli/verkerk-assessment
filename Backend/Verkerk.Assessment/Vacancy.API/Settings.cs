﻿namespace Vacancy.API
{
    public class Settings
    {
        public string[] AllowedCorsDomains { get; set; }
        public JwtSettings JwtSettings { get; set; }
    }

    public class JwtSettings
    {
        public string SecurityKey { get; set; }
        public string Issuer { get; set; }
    }
}