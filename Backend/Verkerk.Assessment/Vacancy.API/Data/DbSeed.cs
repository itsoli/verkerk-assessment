﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vacancy.API.Context;
using Vacancy.API.Domain;
using Vacancy = Vacancy.API.Domain.Vacancy;

namespace Vacancy.API.Data
{
    public class DbSeed
    {
        public static void Initialize(ApplicationDbContext context)
        {
            try
            {
                if (!context.Companies.Any())
                {
                    var verkerk = context.Companies.Add(new Company("Verkerk",
                        "Sustainable, comfortable and safe buildings are a requirement today. In this way, real estate owners, companies and institutions keep their end users satisfied, and they can perform optimally. Buildings and sites are functionally brought to life through the well thought-out building solutions of Verkerk and our partners. Our technical installations, solutions and services enhance environments from the entrance gate. ​Our goal? Bringing buildings to life!",
                        "+31 (0)78 610 78 00", "Netherlands", "Rotterdam", "Rotterdam", " Rotterdam 68740883",
                        "NL857571217.B.01"));

                    context.SaveChanges();


                    context.VacancyCategories.AddRange(new List<VacancyCategory>()
                    {
                        new VacancyCategory("IT", "1"),
                        new VacancyCategory("HR", "2")
                    });

                    context.SaveChanges();


                    context.Vacancies.AddRange(new List<Domain.Vacancy>
                    {
                        new Domain.Vacancy("Marketing & Communications Advisor - Zwijndrecht", "1",
                            "As an ambassador, will you help shape and expand Verkerk as a brand and realize our ambitions? We are looking for a  Marketing & Communications Consultant! ",
                            PositionLevel.Experienced, verkerk.Entity.SequentialId, 1),
                        new Domain.Vacancy("HR Business Partner - Zwijndrecht", "2",
                            "As an HR Business Partner you advise and support the management of the Business Units  (BU) in the implementation of the HR and organizational policy and you act as a sparring partner!",
                            PositionLevel.Senior, verkerk.Entity.SequentialId, 2)
                    });

                    context.SaveChanges();
                }
            }
            catch (Exception)
            {
                //ignore               
            }
        }
    }
}