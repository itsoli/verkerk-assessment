﻿using System.Collections;
using System.Collections.Generic;
using Verkerk.Common.Core.Domain;

namespace Vacancy.API.Domain
{
    public class VacancyCategory : Entity
    {
        public VacancyCategory()
        {
            
        }

        public VacancyCategory(string name, string code)
        {
            Name = name;
            Code = code;
        }
        public string Name { get; set; }
        public string Code { get; set; }

        public ICollection<Vacancy> Vacancies { get; set; }
    }
}