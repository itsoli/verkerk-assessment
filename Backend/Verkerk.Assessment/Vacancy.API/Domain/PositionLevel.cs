﻿namespace Vacancy.API.Domain
{
    public enum PositionLevel
    {
        Entry=1,
        Experienced,
        Senior,
        Directors,
        VicePresidents
    }
}