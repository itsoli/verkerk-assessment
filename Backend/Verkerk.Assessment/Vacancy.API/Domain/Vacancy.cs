﻿using Verkerk.Common.Core.Domain;

namespace Vacancy.API.Domain
{
    public class Vacancy : Entity
    {
        public Vacancy()
        {
        }

        public Vacancy(string title, string code, string description, PositionLevel positionLevel, long companyId,
            long categoryId)
        {
            Title = title;
            Code = code;
            Description = description;
            PositionLevel = positionLevel;
            CompanyId = companyId;
            CategoryId = categoryId;
        }

        public void Update(string title, string code, string description, PositionLevel positionLevel, long companyId,
            long categoryId)
        {
            Title = title;
            Code = code;
            Description = description;
            PositionLevel = positionLevel;
            CompanyId = companyId;
            CategoryId = categoryId;
        }

        public string Title { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public PositionLevel PositionLevel { get; set; }
        public long CompanyId { get; set; }
        public Company Company { get; set; }
        public long CategoryId { get; set; }
        public VacancyCategory Category { get; set; }
    }
}