﻿using System.Collections;
using System.Collections.Generic;
using Verkerk.Common.Core.Domain;

namespace Vacancy.API.Domain
{
    public class Company : Entity
    {
        public Company()
        {
        }

        public Company(string name, string description, string phoneNumber, string country, string province,
            string city, string address, string zipCode)
        {
            Name = name;
            Description = description;
            PhoneNumber = phoneNumber;
            Country = country;
            Province = province;
            City = city;
            Address = address;
            ZipCode = zipCode;
        }

        public void Update(string name, string description, string phoneNumber, string country, string province,
            string city, string address, string zipCode)
        {
            Name = name;
            Description = description;
            PhoneNumber = phoneNumber;
            Country = country;
            Province = province;
            City = city;
            Address = address;
            ZipCode = zipCode;
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string PhoneNumber { get; set; }

        // In production software country, province and city should be separate entities, I used `string` for simplicity
        public string Country { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }

        public ICollection<Vacancy> Vacancies { get; set; }
    }
}