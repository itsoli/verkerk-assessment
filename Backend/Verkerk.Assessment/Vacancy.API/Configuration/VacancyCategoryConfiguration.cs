﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Vacancy.API.Configuration
{
    public class VacancyCategoryConfiguration : IEntityTypeConfiguration<Domain.VacancyCategory>
    {
        public void Configure(EntityTypeBuilder<Domain.VacancyCategory> builder)
        {
            builder.ToTable("VacancyCategories");
            builder.HasIndex(p => p.Id).IsUnique();
            builder.HasKey(p => p.SequentialId);
            builder.Property(p => p.SequentialId).ValueGeneratedOnAdd();
            builder.Property(p => p.Code).HasMaxLength(50);
        }
    }
}