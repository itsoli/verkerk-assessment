﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Vacancy.API.Domain;

namespace Vacancy.API.Configuration
{
    public class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.ToTable("Companies");
            builder.HasIndex(p => p.Id).IsUnique();
            builder.HasKey(p => p.SequentialId);
            builder.Property(p => p.SequentialId).ValueGeneratedOnAdd();
            builder.Property(p => p.Name).HasMaxLength(200);
            builder.Property(p => p.Address).HasMaxLength(500);
            builder.Property(p => p.Description).HasMaxLength(500);
            builder.Property(p => p.Country).HasMaxLength(200);
            builder.Property(p => p.City).HasMaxLength(200);
            builder.Property(p => p.Province).HasMaxLength(200);
            builder.Property(p => p.ZipCode).HasMaxLength(50);

            builder.HasMany(a => a.Vacancies)
                .WithOne(s => s.Company)
                .HasForeignKey(s => s.CompanyId);
        }
    }
}