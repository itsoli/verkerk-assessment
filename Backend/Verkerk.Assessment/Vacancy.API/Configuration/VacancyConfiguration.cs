﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Vacancy.API.Domain;
using Verkerk.Common.Core.Domain;

namespace Vacancy.API.Configuration
{
    public class VacancyConfiguration : IEntityTypeConfiguration<Domain.Vacancy>
    {
        public void Configure(EntityTypeBuilder<Domain.Vacancy> builder)
        {
            builder.ToTable("Vacancies");
            builder.HasIndex(p => p.Id).IsUnique();
            builder.HasKey(p => p.SequentialId);
            builder.Property(p => p.SequentialId).ValueGeneratedOnAdd();
            builder.Property(p => p.Title).HasMaxLength(200);
            builder.Property(p => p.Description).HasMaxLength(500);
            builder.Property(p => p.Code).HasMaxLength(50);

            builder.HasOne(s => s.Company)
                .WithMany(q => q.Vacancies)
                .HasForeignKey(q => q.CompanyId);

            builder.HasOne(s => s.Category)
                .WithMany(q => q.Vacancies)
                .HasForeignKey(q => q.CategoryId);
        }
    }
}