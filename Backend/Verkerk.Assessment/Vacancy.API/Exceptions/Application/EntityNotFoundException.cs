﻿using System;
using Verkerk.Common.Core.Exceptions;

namespace Vacancy.API.Exceptions.Application
{
    public class EntityNotFoundException : Exception, IApplicationException
    {
        public EntityNotFoundException()
        {
        }

        public EntityNotFoundException(string message)
            : base(message)
        {
        }

        public EntityNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}