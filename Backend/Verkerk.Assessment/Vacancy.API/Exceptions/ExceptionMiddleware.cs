﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Verkerk.Common.Core.Exceptions;

namespace Vacancy.API.Exceptions
{
    public class ExceptionMiddleware
    {
        private readonly ILogger<ExceptionMiddleware> _logger;
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                _logger.LogError($"An error occurred while processing request: {ex}");
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";


            if (exception is IApplicationException)
                context.Response.StatusCode = (int) HttpStatusCode.Conflict;
            else

                context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;


            return context.Response.WriteAsync(new ErrorDetails
            {
                HResult = exception.HResult,
                StatusCode = context.Response.StatusCode,
                Message = $"An error occurred while processing your request. {exception.Message}"
            }.ToString());
        }
    }
}