﻿using System.Runtime.Serialization;
using Vacancy.API.Domain;

namespace Vacancy.API.Dto
{
    [DataContract]
    public class VacancyDto
    {
        public VacancyDto()
        {
        }

        public VacancyDto(string title, string code, string description, string positionLevel, CompanyDto company,
            VacancyCategoryDto category)
        {
            Title = title;
            Code = code;
            Description = description;
            PositionLevel = positionLevel;
            Company = company;
            Category = category;
        }

        public static VacancyDto FromVacancy(Domain.Vacancy vacancy)
        {
            if (vacancy == null)
                return new VacancyDto();
            return new VacancyDto(vacancy.Title, vacancy.Code, vacancy.Description, vacancy.PositionLevel.ToString(),
                CompanyDto.FromCompany(vacancy.Company), VacancyCategoryDto.FromVacancyCategory(vacancy.Category));
        }

        [DataMember] public string Title { get; set; }
        [DataMember] public string Code { get; set; }
        [DataMember] public string Description { get; set; }
        [DataMember] public string PositionLevel { get; set; }
        [DataMember] public CompanyDto Company { get; set; }
        [DataMember] public VacancyCategoryDto Category { get; set; }
    }
}