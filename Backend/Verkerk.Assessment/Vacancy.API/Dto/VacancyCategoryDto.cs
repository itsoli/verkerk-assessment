﻿using System.Runtime.Serialization;
using Vacancy.API.Domain;

namespace Vacancy.API.Dto
{
    [DataContract]
    public class VacancyCategoryDto
    {
        public VacancyCategoryDto()
        {
        }

        public VacancyCategoryDto(string name, string code)
        {
            Name = name;
            Code = code;
        }

        public static VacancyCategoryDto FromVacancyCategory(VacancyCategory vacancyCategory)
        {
            if (vacancyCategory == null)
                return new VacancyCategoryDto();
            return new VacancyCategoryDto(vacancyCategory.Name, vacancyCategory.Code);
        }

        [DataMember] public string Name { get; set; }
        [DataMember] public string Code { get; set; }
    }
}