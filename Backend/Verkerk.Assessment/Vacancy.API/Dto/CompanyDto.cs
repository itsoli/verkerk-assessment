﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Vacancy.API.Dto
{
    [DataContract]
    public class CompanyDto
    {
        public CompanyDto()
        {
        }

        public CompanyDto(string name, string description, string phoneNumber, string country, string province,
            string city, string address, string zipCode)
        {
            Name = name;
            Description = description;
            PhoneNumber = phoneNumber;
            Country = country;
            Province = province;
            City = city;
            Address = address;
            ZipCode = zipCode;
        }


        public static CompanyDto FromCompany(Domain.Company company)
        {
            if (company == null)
                return new CompanyDto();
            return new CompanyDto(company.Name, company.Description, company.PhoneNumber, company.Country,
                company.Province, company.City, company.Address, company.ZipCode);
        }

        [DataMember] public string Name { get; set; }
        [DataMember] public string Description { get; set; }
        [DataMember] public string PhoneNumber { get; set; }
        [DataMember] public string Country { get; set; }
        [DataMember] public string Province { get; set; }
        [DataMember] public string City { get; set; }
        [DataMember] public string Address { get; set; }
        [DataMember] public string ZipCode { get; set; }
    }
}