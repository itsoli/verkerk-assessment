﻿using System.ComponentModel.DataAnnotations;

namespace Vacancy.API.Models
{
    public class CompanyRequestModel
    {
        public CompanyRequestModel()
        {
            
        }

        public CompanyRequestModel(string name, string description, string phoneNumber, string country, string province, string city, string address, string zipCode)
        {
            Name = name;
            Description = description;
            PhoneNumber = phoneNumber;
            Country = country;
            Province = province;
            City = city;
            Address = address;
            ZipCode = zipCode;
        }
        [Required] public string Name { get; set; }
        public string Description { get; set; }
        public string PhoneNumber { get; set; }

        // In production software country, province and city should be separate entities, I used `string` for simplicity
        [Required] public string Country { get; set; }
        [Required] public string Province { get; set; }
        [Required] public string City { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
    }
}