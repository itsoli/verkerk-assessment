﻿using System;
using System.ComponentModel.DataAnnotations;
using Vacancy.API.Domain;

namespace Vacancy.API.Models
{
    public class VacancyRequestModel
    {
        [Required] public string Title { get; set; }
        [Required] public string Code { get; set; }
        public string Description { get; set; }
        [Required] public PositionLevel PositionLevel { get; set; }
        [Required] public Guid CompanyId { get; set; }
        [Required] public Guid CategoryId { get; set; }
    }
}