﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Vacancy.API.Dto;
using Vacancy.API.Facade;
using Vacancy.API.Models;
using Verkerk.Common.AspNetCore.Mvc;


namespace Vacancy.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = "ManageCompanies")]
    public class CompanyController : ApiControllerBase
    {
        private readonly ICompanyFacade _companyFacade;

        public CompanyController(ICompanyFacade companyFacade)
        {
            _companyFacade = companyFacade;
        }

        /// <summary>
        /// return all companies, the user should be authenticated
        /// </summary>
        /// <returns>companies list</returns>
        [HttpGet]
        public ActionResult<IEnumerable<CompanyDto>> Get()
        {
            var companies = _companyFacade.Get().Select(CompanyDto.FromCompany).ToList();
            return Ok(companies);
        }

        /// <summary>
        /// get details of a single company
        /// </summary>
        /// <param name="id">company id</param>
        /// <returns>company details (CompanyDto)</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        public ActionResult<CompanyDto> Get(Guid id)
        {
            var company = _companyFacade.Get(id);
            return Ok(CompanyDto.FromCompany(company));
        }

        /// <summary>
        /// create a new company 
        /// </summary>
        /// <param name="request">put required data on body of request</param>
        /// <returns>the created company</returns>
        [HttpPost]
        public ActionResult<CompanyDto> Post([FromBody] CompanyRequestModel request)
        {
            if (!ModelState.IsValid)
                return BadRequest("Input data is not valid");

            return Ok(CompanyDto.FromCompany(_companyFacade.Create(request)));
        }

        /// <summary>
        /// update a  company 
        /// </summary>
        /// <param name="id">company id</param>
        /// <param name="request">put required data on body of request</param>
        /// <returns>the updated company</returns>
        [HttpPut("{id}")]
        public ActionResult<CompanyDto> Put(Guid id, [FromBody] CompanyRequestModel request)
        {
            if (!ModelState.IsValid)
                return BadRequest("Input data is not valid");

            return Ok(CompanyDto.FromCompany(_companyFacade.Update(id, request)));
        }

        /// <summary>
        /// delete a company
        /// </summary>
        /// <param name="id">company id</param>
        /// <returns>the id of deleted company</returns>
        [HttpDelete("{id}")]
        public ActionResult<Guid> Delete(Guid id)
        {
            return Ok(_companyFacade.Delete(id));
        }
    }
}