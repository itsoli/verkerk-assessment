﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Vacancy.API.Dto;
using Vacancy.API.Facade;
using Vacancy.API.Models;
using Verkerk.Common.AspNetCore.Mvc;


namespace Vacancy.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = "ManageVacancies")]
    public class VacanciesController : ApiControllerBase
    {
        private readonly IVacancyFacade _vacancyFacade;

        public VacanciesController(IVacancyFacade vacancyFacade)
        {
            _vacancyFacade = vacancyFacade;
        }

        /// <summary>
        /// get list of all available vacancies
        /// </summary>
        /// <returns>all available vacancies</returns>
        [HttpGet]
        [AllowAnonymous]
        public IEnumerable<VacancyDto> Get()
        {
            return _vacancyFacade.Get().Select(VacancyDto.FromVacancy).ToList();
        }


        /// <summary>
        /// get available vacancies of a single company
        /// </summary>
        /// <param name="id">company id</param>
        /// <returns>available vacancies of a single company</returns>
        [HttpGet("Company/{id}")]
        [AllowAnonymous]
        public IEnumerable<VacancyDto> GetByCompanyId(Guid id)
        {
            return _vacancyFacade.GetByCompany(id).Select(VacancyDto.FromVacancy).ToList();
        }

        /// <summary>
        /// get a single vacancy details
        /// </summary>
        /// <param name="id">vacancy id</param>
        /// <returns>single vacancy details object</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        public VacancyDto Get(Guid id)
        {
            return VacancyDto.FromVacancy(_vacancyFacade.Get(id));
        }

        /// <summary>
        /// create a new vacancy for a company
        /// </summary>
        /// <param name="request">send required data in body of request</param>
        /// <returns>the created vacancy</returns>
        [HttpPost]
        public ActionResult<VacancyDto> Post([FromBody] VacancyRequestModel request)
        {
            if (!ModelState.IsValid)
                return BadRequest("Input data is not valid");

            return VacancyDto.FromVacancy(_vacancyFacade.Create(request));
        }

        /// <summary>
        /// update a vacancy
        /// </summary>
        /// <param name="id">vacancy id</param>
        /// <param name="request">send required data in body of request</param>
        /// <returns>the updated vacancy</returns>
        [HttpPut("{id}")]
        public ActionResult<VacancyDto> Put(Guid id, [FromBody] VacancyRequestModel request)
        {
            if (!ModelState.IsValid)
                return BadRequest("Input data is not valid");

            return VacancyDto.FromVacancy(_vacancyFacade.Update(id, request));
        }

        /// <summary>
        /// delete a vacancy
        /// </summary>
        /// <param name="id">vacancy id</param>
        /// <returns>the id of deleted vacancy</returns>
        [HttpDelete("{id}")]
        public ActionResult<Guid> Delete(Guid id)
        {
            return _vacancyFacade.Delete(id);
        }
    }
}