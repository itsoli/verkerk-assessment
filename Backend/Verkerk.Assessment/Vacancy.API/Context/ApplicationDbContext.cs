﻿using Microsoft.EntityFrameworkCore;
using Vacancy.API.Configuration;
using Vacancy.API.Domain;

namespace Vacancy.API.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Company> Companies { get; set; }
        public DbSet<Domain.Vacancy> Vacancies { get; set; }
        public DbSet<Domain.VacancyCategory> VacancyCategories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //apply configurations to context
            modelBuilder.ApplyConfiguration(new CompanyConfiguration());
            modelBuilder.ApplyConfiguration(new VacancyConfiguration());
            modelBuilder.ApplyConfiguration(new VacancyCategoryConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}