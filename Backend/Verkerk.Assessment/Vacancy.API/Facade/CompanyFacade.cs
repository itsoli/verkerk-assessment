﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Formatters;
using Vacancy.API.Context;
using Vacancy.API.Domain;
using Vacancy.API.Exceptions.Application;
using Vacancy.API.Models;

namespace Vacancy.API.Facade
{
    public class CompanyFacade : ICompanyFacade
    {
        private readonly ApplicationDbContext _context;

        public CompanyFacade(ApplicationDbContext context)
        {
            _context = context;
        }


        public IEnumerable<Company> Get()
        {
            return _context.Companies.ToList();
        }

        public Company Get(Guid id)
        {
            var company = _context.Companies.FirstOrDefault(s => s.Id.Equals(id));
            if (company == null)
                throw new EntityNotFoundException("Company was not found");
            return company;
        }

        public Company Create(CompanyRequestModel request)
        {
            var company = _context.Companies.Add(new Company(request.Name, request.Description, request.PhoneNumber,
                request.Country, request.Province, request.City, request.Address, request.ZipCode));
            _context.SaveChanges();
            return company.Entity;
        }

        public Company Update(Guid id, CompanyRequestModel request)
        {
            var company = _context.Companies.FirstOrDefault(s => s.Id.Equals(id));
            if (company == null)
                throw new EntityNotFoundException("Company was not found");

            company.Update(request.Name, request.Description, request.PhoneNumber,
                request.Country, request.Province, request.City, request.Address, request.ZipCode);
            _context.SaveChanges();
            return company;
        }

        public Guid Delete(Guid id)
        {
            //it's better to safe delete the entity, this is for demo only
            var company = _context.Companies.FirstOrDefault(s => s.Id.Equals(id));
            if (company == null)
                throw new EntityNotFoundException("Company was not found");

            _context.Companies.Remove(company);
            _context.SaveChanges();

            return id;
        }
    }
}