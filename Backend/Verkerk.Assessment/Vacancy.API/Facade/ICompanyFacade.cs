﻿using System;
using System.Collections.Generic;
using Vacancy.API.Domain;
using Vacancy.API.Dto;
using Vacancy.API.Models;

namespace Vacancy.API.Facade
{
    public interface ICompanyFacade
    {
        public IEnumerable<Company> Get();
        public Company Get(Guid id);
        public Company Create(CompanyRequestModel request);
        public Company Update(Guid id, CompanyRequestModel request);
        public Guid Delete(Guid id);
    }
}