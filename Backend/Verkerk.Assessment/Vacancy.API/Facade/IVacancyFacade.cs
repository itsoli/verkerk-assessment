﻿using System;
using System.Collections.Generic;
using Vacancy.API.Models;

namespace Vacancy.API.Facade
{
    public interface IVacancyFacade
    {
        public IEnumerable<Domain.Vacancy> Get();
        public IEnumerable<Domain.Vacancy> GetByCompany(Guid companyId);
        public Domain.Vacancy Get(Guid id);
        public Domain.Vacancy Create(VacancyRequestModel request);
        public Domain.Vacancy Update(Guid id, VacancyRequestModel request);
        public Guid Delete(Guid id);
    }
}