﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Vacancy.API.Context;
using Vacancy.API.Exceptions.Application;
using Vacancy.API.Models;

namespace Vacancy.API.Facade
{
    public class VacancyFacade : IVacancyFacade
    {
        private readonly ApplicationDbContext _context;

        public VacancyFacade(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Domain.Vacancy> Get()
        {
            return _context.Vacancies
                .Include(s => s.Category)
                .Include(s => s.Company)
                .OrderByDescending(s => s.CreateTimeUtc).ToList();
        }

        public Domain.Vacancy Get(Guid id)
        {
            var vacancy = _context.Vacancies.Include(s => s.Category)
                .Include(s => s.Company).FirstOrDefault(s => s.Id.Equals(id));
            if (vacancy == null)
                throw new EntityNotFoundException("Vacancy was not found");
            return vacancy;
        }

        public IEnumerable<Domain.Vacancy> GetByCompany(Guid companyId)
        {
            var company = _context.Companies.FirstOrDefault(s => s.Id == companyId);

            if (company == null)
                throw new EntityNotFoundException("Company was not found");

            return _context.Vacancies.Include(s => s.Category)
                .Include(s => s.Company).OrderByDescending(s => s.CreateTimeUtc)
                .Where(s => s.CompanyId == company.SequentialId).ToList();
        }

        public Domain.Vacancy Create(VacancyRequestModel request)
        {
            var company = _context.Companies.FirstOrDefault(s => s.Id == request.CompanyId);

            if (company == null)
                throw new EntityNotFoundException("Company was not found");

            var category = _context.VacancyCategories.FirstOrDefault(s => s.Id == request.CategoryId);

            if (category == null)
                throw new EntityNotFoundException("Category was not found");


            var vacancy = _context.Vacancies.Add(new Domain.Vacancy(request.Title, request.Code, request.Description,
                    request.PositionLevel, company.SequentialId, category.SequentialId));

            _context.SaveChanges();
            return vacancy.Entity;
        }

        public Domain.Vacancy Update(Guid id, VacancyRequestModel request)
        {
            var vacancy = _context.Vacancies.Include(s => s.Category)
                .Include(s => s.Company).FirstOrDefault(s => s.Id.Equals(id));

            if (vacancy == null)
                throw new EntityNotFoundException(" Vacancy was not found");

            var company = _context.Companies.FirstOrDefault(s => s.Id == request.CompanyId);

            if (company == null)
                throw new EntityNotFoundException("Company was not found");

            var category = _context.VacancyCategories.FirstOrDefault(s => s.Id == request.CategoryId);

            if (category == null)
                throw new EntityNotFoundException("Category was not found");


            vacancy.Update(request.Title, request.Code, request.Description, request.PositionLevel,
                company.SequentialId, category.SequentialId);

            _context.SaveChanges();
            return vacancy;
        }

        public Guid Delete(Guid id)
        {
            //it's better to safe delete the entity, this is for demo only
            var vacancy = _context.Vacancies.FirstOrDefault(s => s.Id.Equals(id));
            if (vacancy == null)
                throw new EntityNotFoundException("Vacancy was not found");

            _context.Vacancies.Remove(vacancy);
            _context.SaveChanges();

            return id;
        }
    }
}