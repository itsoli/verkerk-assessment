﻿using System.ComponentModel.DataAnnotations;

namespace Identity.API.Models.Authentication
{
    public class TokenRequestModel
    {
        [Required] public string Username { get; set; }
        [Required] public string Password { get; set; }
    }
}