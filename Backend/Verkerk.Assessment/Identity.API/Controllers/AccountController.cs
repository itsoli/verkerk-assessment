﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Identity.API.Dto;
using Identity.API.Models.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Verkerk.Common.Core.Security;

namespace Identity.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly Settings _settings;

        //for demo only
        private const string UserId = "82830087-9bcf-40f0-b583-adc8a8e5b680";

        public AccountController(IOptions<Settings> settings)
        {
            _settings = settings.Value;
        }

        /// <summary>
        /// Request a token base on username and password, for testing just send same username and password!
        /// </summary>
        /// <param name="request">a json object that contains username and password</param>
        /// <returns>jwt token with expiration date</returns>
        [HttpPost("token"), AllowAnonymous]
        public ActionResult<TokenResponseDto> RequestToken([FromBody] TokenRequestModel request)
        {
            if (!ModelState.IsValid) return BadRequest();

            var user = Authenticate(request.Username, request.Password);

            if (user == null) return Unauthorized();

            var token = GetJwtSecurityToken(user);

            return Ok(new TokenResponseDto(new JwtSecurityTokenHandler().WriteToken(token), token.ValidTo));
        }

        private static UserModel Authenticate(string username, string password)
        {
            // For simple authentication we just compare username and password
            return username.Equals(password)
                ? new UserModel
                {
                    Id = Guid.Parse(UserId),
                    Name = "Joe Soap",
                    Email = "admin@verkerk.com"
                }
                : default;
        }

        private JwtSecurityToken GetJwtSecurityToken(UserModel user)
        {
            //add claims to jwt token
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Role, "CanManageCompanies"),
                new Claim(ClaimTypes.Role, "CanManageVacancies"),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_settings.JwtSettings.SecurityKey));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            //generate token
            var token = new JwtSecurityToken(_settings.JwtSettings.Issuer, _settings.JwtSettings.Issuer,
                claims,
                expires: DateTime.UtcNow.AddHours(6),
                signingCredentials: credentials);

            return token;
        }
    }
}