﻿using System;
using System.Runtime.Serialization;

namespace Identity.API.Dto
{
    [DataContract]
    public class TokenResponseDto
    {
        public TokenResponseDto()
        {
        }

        public TokenResponseDto(string token, DateTimeOffset expiration)
        {
            Token = token;
            ExpirationUtc = expiration;
        }

        [DataMember] public string Token { get; set; }
        [DataMember] public DateTimeOffset ExpirationUtc { get; set; }
    }
}