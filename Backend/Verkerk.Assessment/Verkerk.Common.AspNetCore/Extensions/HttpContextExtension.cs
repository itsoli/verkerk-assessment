﻿using System;
using System.Linq;
using System.Security.Claims;
using Verkerk.Common.Core.Models;
using Verkerk.Common.Core.Security;

namespace Verkerk.Common.AspNetCore.Extensions
{
    public static class HttpContextExtension
    {
        //An extension method to get and userModel from jwt data
        public static UserModel GetUserDataFromJwtToken(this ClaimsPrincipal claimsPrincipal)
        {
            try
            {
                var identity = claimsPrincipal.Identities
                    .OrderByDescending(s => s.Claims.Count()).FirstOrDefault();

                if (identity == null)
                    return null;

                //read claims
                var id =
                    identity.Claims.FirstOrDefault(s =>
                        s.Type.Equals(ClaimTypes.NameIdentifier));
                var companyId =
                    identity.Claims.FirstOrDefault(s =>
                        s.Type.Equals(VerkerkClaimTypes.CompanyId));
                var name = identity.Claims.FirstOrDefault(s =>
                    s.Type.Equals(ClaimTypes.Name));

                var email = identity.Claims.FirstOrDefault(s =>
                    s.Type.Equals(ClaimTypes.Email));


                return new UserModel(Guid.Parse(id?.Value), name?.Value, email?.Value);
            }
            catch (Exception)
            {
                //we couldn't read the token!
                return null;
            }
        }
    }
}