﻿using System;
using Microsoft.AspNetCore.Mvc;
using Verkerk.Common.AspNetCore.Extensions;
using Verkerk.Common.Core.Models;

namespace Verkerk.Common.AspNetCore.Mvc
{
    //A simple ControllerBase that has a UserModel!
    public class ApiControllerBase : ControllerBase
    {
        protected UserModel VerkerkUser
        {
            get
            {
                try
                {
                    return HttpContext.User.GetUserDataFromJwtToken();
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}