﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Vacancy.API.Controllers;
using Vacancy.API.Facade;
using Vacancy.API.Models;
using Verkerk.Common.Core.Models;
using Xunit;

namespace Vacancy.UnitTests.Application
{
    public class CompanyControllerTests
    {
        private readonly Mock<ICompanyFacade> _companyFacade;

        public CompanyControllerTests()
        {
            _companyFacade = new Mock<ICompanyFacade>();
        }

        [Fact]
        public void Get_Companies_200()
        {
            //arrange

            var garageController = GetCompanyController();

            //Act
            var actionResult = garageController.Get();

            //Assert
            Assert.IsType<OkObjectResult>(actionResult.Result);
        }


        [Fact]
        public void Get_Company_By_Id_200()
        {
            //arrange
            var companyController = GetCompanyController();

            //Act
            var actionResult = companyController.Get(Guid.NewGuid());

            //Assert
            Assert.IsType<OkObjectResult>(actionResult.Result);
        }


        [Fact]
        public void Create_Company_200()
        {
            //arrange
            var companyController = GetCompanyController();

            //Act
            var actionResult = companyController.Post(new CompanyRequestModel("Test company 1", "Test Description 1",
                "+989366661583", "Iran", "Tehran", "Tehran", "WEST", "123456"));

            //Assert
            Assert.IsType<OkObjectResult>(actionResult.Result);
        }

        [Fact]
        public void Update_Company_200()
        {
            //arrange
            var companyController = GetCompanyController();

            //Act
            var actionResult = companyController.Put(Guid.NewGuid(),new CompanyRequestModel("Test company 1", "Test Description 1",
                "+989366661583", "Iran", "Tehran", "Tehran", "WEST", "123456"));
            //Assert
            Assert.IsType<OkObjectResult>(actionResult.Result);
        }


        [Fact]
        public void Delete_Company_200()
        {
            //arrange
            var companyController = GetCompanyController();

            //Act
            var actionResult = companyController.Delete(Guid.NewGuid());
            //Assert
            Assert.IsType<OkObjectResult>(actionResult.Result);
        }


        private CompanyController GetCompanyController()
        {
            var httpContext = new DefaultHttpContext();

            var companyController = new CompanyController(_companyFacade.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext
                }
            };

            return companyController;
        }
    }
}