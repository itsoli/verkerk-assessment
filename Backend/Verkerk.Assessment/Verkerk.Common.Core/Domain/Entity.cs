﻿using System;

namespace Verkerk.Common.Core.Domain
{
    public class Entity
    {
        // this class can be a nuget package to be used across different projects

        public Entity()
        {
            Id = Guid.NewGuid();
            CreateTimeUtc = DateTimeOffset.UtcNow;
            CreateTimeLocal = DateTimeOffset.Now;
        }

        public Guid Id { get; set; }
        public long SequentialId { get; set; }
        public DateTimeOffset CreateTimeUtc { get; set; }
        public DateTimeOffset CreateTimeLocal { get; set; }
    }
}