﻿using System;

namespace Verkerk.Common.Core.Models
{
    public class UserModel
    {
        public UserModel()
        {
        }

        public UserModel(Guid id, string name, string email)
        {
            Id = id;
            Name = name;
            Email = email;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}