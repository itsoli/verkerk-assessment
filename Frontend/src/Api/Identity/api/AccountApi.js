import { ApiClient } from '../../ApiClient'
import { TokenRequestModel } from '../model/TokenRequestModel'
import { TokenResponseDto } from '../model/TokenResponseDto'

/**
 * Account service.
 * @module api/AccountApi
 * @version v1
 */
export class AccountApi {
  /**
    * Constructs a new AccountApi. 
    * @alias module:api/AccountApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instanc
    e} if unspecified.
    */
  constructor(apiClient) {
    this.apiClient = apiClient || ApiClient.instance
  }

  /**
   * Callback function to receive the result of the apiAccountTokenPost operation.
   * @callback moduleapi/AccountApi~apiAccountTokenPostCallback
   * @param {String} error Error message, if any.
   * @param {module:model/TokenResponseDto{ data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * @param {Object} opts Optional parameters
   * @param {module:model/TokenRequestModel} opts.body
   * @param {module:api/AccountApi~apiAccountTokenPostCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link <&vendorExtensions.x-jsdoc-type>}
   */
  apiAccountTokenPost(opts, callback) {
    opts = opts || {}
    let postBody = opts['body']

    let pathParams = {}
    let queryParams = {}
    let headerParams = {}
    let formParams = {}

    let authNames = []
    let contentTypes = ['application/json', 'text/json', 'application/_*+json']
    let accepts = ['text/plain', 'application/json', 'text/json']
    let returnType = TokenResponseDto

    return this.apiClient.callApi(
      '/api/Account/token',
      'POST',
      pathParams,
      queryParams,
      headerParams,
      formParams,
      postBody,
      authNames,
      contentTypes,
      accepts,
      returnType,
      callback
    )
  }
}
