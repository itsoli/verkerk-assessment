/*eslint-disable*/

// reactstrap components
import { Container, Row, Col, Nav, NavItem, NavLink } from 'reactstrap'

const Footer = () => {
  return (
    <footer className="footer">
      <Row className="align-items-center justify-content-xl-between">
        <Col xl="6">
          <div className="copyright text-center text-xl-left text-muted">
            © {new Date().getFullYear()}{' '}
            <a
              className="font-weight-bold ml-1"
              href="https://verkerk.com/"
              rel="noopener noreferrer"
              target="_blank"
            >
              All rights reserved @Verkerk
            </a>
          </div>
        </Col>
      </Row>
    </footer>
  )
}

export default Footer
