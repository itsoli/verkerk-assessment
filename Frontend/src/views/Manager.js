import { useState, useEffect } from 'react'

import {
  Button,
  Card,
  CardHeader,
  Table,
  Container,
  Row,
  Col
} from 'reactstrap'
import { useToasts } from 'react-toast-notifications'
import { TokenRequestModel, AccountApi } from 'Api/Identity'
import { CompanyApi } from 'Api/Vacancy'
import { ApiClient } from 'Api'

const Index = (props) => {
  const [companies, setCompanies] = useState([])
  const { addToast } = useToasts()
  const apiClinet = new ApiClient()

  useEffect(() => {
    async function GetCompanies() {
      apiClinet.basePath = process.env.REACT_APP_API_BASE
      var api = new CompanyApi(apiClinet)
      var callback = function (error, data, response) {
        if (error) {
          addToast(
            "Couldn't connect to server please check your internet connection or run backend app",
            { appearance: 'error' }
          )
          console.error(error)
        } else {
          setCompanies(data)
        }
      }
      api.apiCompanyGet(callback)
    }

    async function Login() {
      apiClinet.basePath = process.env.REACT_APP_API_AUTH
      var api = new AccountApi(apiClinet)
      const opts = {
        body: new TokenRequestModel('test', 'test')
      }
      var callback = function (error, data, response) {
        if (error) {
          addToast(
            "Couldn't connect to server please check your internet connection or run backend app",
            { appearance: 'error' }
          )
          console.error(error)
        } else {
          localStorage.setItem('accessToken', data.token)
          GetCompanies()
        }
      }
      api.apiAccountTokenPost(opts, callback)
    }

    Login()
  }, [])

  return (
    <>
      <Container className="mt-7" fluid>
        <Row>
          <Col>
            <Card className="shadow">
              <CardHeader className="border-0">
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Companies</h3>
                  </div>
                </Row>
              </CardHeader>

              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th
                      style={{ whiteSpace: 'break-spaces' }}
                      colSpan={2}
                      scope="row"
                    >
                      Name
                    </th>
                    <th style={{ whiteSpace: 'break-spaces' }} colSpan={2}>
                      Description
                    </th>
                    <th>PhoneNumber</th>
                    <th>Country</th>
                    <th>Province</th>
                    <th>City</th>
                    <th>Address</th>
                    <th>ZipCode</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {companies.map((item) => (
                    <tr>
                      <th
                        style={{ whiteSpace: 'break-spaces' }}
                        colSpan={2}
                        scope="row"
                      >
                        {item.name}
                      </th>
                      <td style={{ whiteSpace: 'break-spaces' }} colSpan={2}>
                        {item.description}
                      </td>
                      <td>{item.phoneNumber}</td>
                      <td>{item.country}</td>
                      <td>{item.province}</td>
                      <td>{item.city}</td>
                      <td>{item.address}</td>
                      <td>{item.zipCode}</td>
                      <td>
                        <Button className="btn btn-sm btn-success">
                          Manage Vacancies
                        </Button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default Index
