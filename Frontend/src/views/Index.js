import { useState, useEffect } from 'react'
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  Table,
  Container,
  Row,
  Col,
  Badge,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap'
import { Link } from 'react-router-dom'

const Index = (props) => {
  return (
    <>
      <Container>
        <Row>
          <Col>
            <Card className=" mt-5 shadow">
              <CardHeader className="border-0">
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">why you are here?</h3>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <Link className="btn btn-primary btn-lg" to="/admin/manager">
                  I'm a Manager
                </Link>
                <Link className="btn btn-success btn-lg" to="/admin/manager">
                  I'm a User
                </Link>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default Index
