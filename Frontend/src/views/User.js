import { useState, useEffect } from 'react'
import WorldMap from './D3NetherlandsMap'

import {
  Button,
  Card,
  CardHeader,
  Table,
  Container,
  Row,
  Col
} from 'reactstrap'
import { useToasts } from 'react-toast-notifications'
import { VacanciesApi } from 'Api/Vacancy'
import { ApiClient } from 'Api'

const User = (props) => {
  const [Vacancies, setVacancies] = useState([])
  const [countries, setCountries] = useState([])

  const { addToast } = useToasts()
  useEffect(() => {
    const apiClinet = new ApiClient()

    async function GetVacancies() {
      apiClinet.basePath = process.env.REACT_APP_API_BASE

      var api = new VacanciesApi(apiClinet)
      var callback = function (error, data, response) {
        if (error) {
          addToast(
            "Couldn't connect to server please check your internet connection or run backend app",
            { appearance: 'error' }
          )
          console.error(error)
        } else {
          setVacancies(data)

          fetch('http://localhost:3000/countries.json').then((response) => {
            if (response.status !== 200) {
              console.log(`There was a problem: ${response.status}`)
              return
            }
            response.json().then((cnt) => {
              data.forEach((element) => {
                var country = cnt.find(
                  (s) => s.name === element.company.country
                )

                ++country.population
              })
              var filtered = cnt.filter((s) => s.population > 0)
              console.log(filtered)
              setCountries(filtered)
            })
          })
        }
      }
      api.apiVacanciesGet(callback)
    }
    GetVacancies()
  }, [])

  return (
    <>
      <Container fluid>
        <Row>
          <Col>
            <WorldMap countries={countries} />
          </Col>
        </Row>
        <Row>
          <Col>
            <Card className="shadow">
              <CardHeader className="border-0">
                <Row className="align-items-center">
                  <div className="col">
                    <h3 className="mb-0">Vacancies</h3>
                  </div>
                </Row>
              </CardHeader>

              <Table size="sm" responsive>
                <thead className="thead-light">
                  <tr>
                    <th
                      style={{ whiteSpace: 'break-spaces' }}
                      colSpan={2}
                      scope="row"
                    >
                      Title
                    </th>
                    <th>Code</th>
                    <th style={{ whiteSpace: 'break-spaces' }} colSpan={2}>
                      Description
                    </th>
                    <th>Position Level</th>
                    <th>Company</th>
                    <th>Category</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {Vacancies.map((item) => (
                    <tr>
                      <th
                        style={{ whiteSpace: 'break-spaces' }}
                        colSpan={2}
                        scope="row"
                      >
                        {item.title}
                      </th>
                      <td>{item.code}</td>
                      <td style={{ whiteSpace: 'break-spaces' }} colSpan={2}>
                        {item.description}
                      </td>
                      <td>{item.positionLevel}</td>
                      <td>{item.company.name}</td>
                      <td>{item.category.name}</td>
                      <td>
                        <Button className="btn btn-sm btn-success">
                          Apply Now
                        </Button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default User
