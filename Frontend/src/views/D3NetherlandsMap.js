import React, { useState, useEffect } from 'react'
import { geoEqualEarth, geoPath } from 'd3-geo'
import { feature } from 'topojson-client'
import { useToasts } from 'react-toast-notifications'

const projection = geoEqualEarth()
  .scale(160)
  .translate([800 / 2, 450 / 2])

const WorldMap = (props) => {
  const [geographies, setGeographies] = useState([])
  const { addToast } = useToasts()

  useEffect(() => {
    fetch(
      'https://raw.githubusercontent.com/vega/datalib/master/test/data/world-110m.json'
    ).then((response) => {
      if (response.status !== 200) {
        console.log(`There was a problem: ${response.status}`)
        return
      }
      response.json().then((worlddata) => {
        setGeographies(feature(worlddata, worlddata.objects.countries).features)
      })
    })
  }, [props])

  const handleCountryClick = (countryIndex) => {
    console.log('Clicked on country: ', geographies[countryIndex])
  }

  const handleMarkerClick = (i) => {
    console.log('Marker: ', props.countries[i])

    addToast(
      `There are ${props.countries[i].population} jobs in ${props.countries[i].name}`,
      { appearance: 'success' }
    )
  }

  return (
    <svg width="100%" height={600} viewBox="0 0 800 200">
      <g className="countries">
        {geographies.map((d, i) => (
          <path
            key={`path-${i}`}
            d={geoPath().projection(projection)(d)}
            className="country"
            fill={`rgba(38,50,56,${(1 / geographies.length) * i})`}
            stroke="#FFFFFF"
            strokeWidth={0.5}
            onClick={() => handleCountryClick(i)}
          />
        ))}
      </g>
      <g className="markers">
        {props.countries.map((city, i) => {
          city.coordinates2 = [city.coordinates[1], city.coordinates[0]]
          return (
            <circle
              key={`marker-${i}`}
              cx={projection(city.coordinates2)[0]}
              cy={projection(city.coordinates2)[1]}
              r={city.population}
              fill="#E91E63"
              stroke="#FFFFFF"
              className="marker"
              onClick={() => handleMarkerClick(i)}
            />
          )
        })}
      </g>
    </svg>
  )
}

export default WorldMap
