import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'

import 'assets/plugins/nucleo/css/nucleo.css'
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'assets/scss/argon-dashboard-react.scss'

import AdminLayout from 'layouts/Admin.js'
import AuthLayout from 'layouts/Auth.js'
import { ToastProvider } from 'react-toast-notifications'

ReactDOM.render(
  <ToastProvider>
    <BrowserRouter>
      <Switch>
        <Route
          path="/admin/index"
          render={(props) => <AdminLayout {...props} />}
        />
        <Route
          path="/admin/user"
          render={(props) => <AdminLayout {...props} />}
        />
        <Route
          path="/admin/manager"
          render={(props) => <AdminLayout {...props} />}
        />

        <Route path="/auth" render={(props) => <AuthLayout {...props} />} />
        <Redirect from="/" to="/admin/index" />
      </Switch>
    </BrowserRouter>
  </ToastProvider>,
  document.getElementById('root')
)
