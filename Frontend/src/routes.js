import Index from 'views/Index.js'
import Manager from 'views/Manager.js'
import User from 'views/User.js'

var routes = [
  {
    path: '/index',
    name: 'why you are here?',
    icon: 'ni ni-tv-2 text-primary',
    component: Index,
    layout: '/admin'
  },
  {
    path: '/manager',
    name: 'Manager Dashboard',
    icon: 'ni ni-tv-2 text-primary',
    component: Manager,
    layout: '/admin'
  },
  {
    path: '/user',
    name: 'User Dashboard',
    icon: 'ni ni-tv-2 text-primary',
    component: User,
    layout: '/admin'
  }
]
export default routes
